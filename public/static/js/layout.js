//DOM加载完毕
$(function(){
  $(".nav-link").click(function(){
    if($(this).parents().is(".dropdowns")){
      if ($(this).siblings().is(".show")) {
        $(this).siblings(".dropdown-menu").removeClass("show");
      }else{
        $(this).siblings(".dropdown-menu").addClass("show");
      }

      
    }
    $(".nav-link").removeAttr("style","")
    $(this).css("background","red");
     //alert($(this).text());
  });


  /*
<span style="white-space:pre">  </span>$('#collapse').addClass("collapsed");
<span style="white-space:pre">  </span>$('#collapse').attr("aria-expanded",false);
<span style="white-space:pre">  </span>$("#navbar").removeClass("in");
<span style="white-space:pre">  </span>$("#navbar").attr("aria-expanded",false);*/

  autoFit();
});

//浏览器窗口的大小发生变化
$(window).resize(function () {
   	autoFit();    
});

// 自适应浏览器窗口大小
function autoFit() {
	// 主容器高度
	var main  = $(window).height()-$("header").height()-$("footer").height();
	$("main").height(main);
  // iframe高度
  $("iframe").height(main-$(".T_tab").height());
  // 左侧导航顶部容器的高度等于右侧TAB容器的高度
  $(".N_top").height($(".T_tab").height());
  // 左侧导航的高度
  $(".left-nav").height(main-$(".N_top").height());
	// 右侧宽度等于窗口宽度减去左侧导航容器的宽度
	var main_right_height = $(window).width()-$(".main-left").width();
	$(".main-right").width(main_right_height);
   	//alert($(window).height());          //浏览器时下窗口可视区域高度
    //alert($(document).height());        //浏览器时下窗口文档的高度
    //alert($(document.body).height());   //浏览器时下窗口文档body的高度
    //alert($(document.body).outerHeight(true)); //浏览器时下窗口文档body的总高度 包括border padding margin
   }